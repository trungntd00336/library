/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lib.da;

import com.lib.enitity.Book;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Shuuyu
 */
public class BookManager {
    private static PreparedStatement Namestmt;
    private static PreparedStatement Idstmt;
    
    private PreparedStatement getNamestmt() throws ClassNotFoundException, SQLException {
        if (Namestmt == null){
            Connection connection = DBConnection.getConnection();
            Namestmt = connection.prepareStatement("select MaSach, [Tensach],[MaTG],[MaLoai],[MaNXB],[Tomtat] from book");
        }
        return Namestmt;
    }
    
    public List<Book> getBooksByName (String keyword){
        try {
            PreparedStatement statement = getNamestmt();
            statement.setString(1, "%" + keyword + "%");
            ResultSet rs = statement.executeQuery();
            List<Book> books = new LinkedList<Book>();
            while (rs.next()){
                int MaSach = rs.getInt("MaSach");
                String Tensach = rs.getString("Tensach");
                String MaLoai = rs.getString("MaLoai");
                String MaTG = rs.getString("MaTG");
                String MaNXB = rs.getString("MaNXB");
                String Tomtat = rs.getString("Tomtat");
                books.add(new Book(MaSach, Tensach, MaLoai, MaTG, MaNXB, Tomtat));
            }
            return books;
        } catch (Exception ex) {
            Logger.getLogger(com.lib.da.BookManager.class.getName()).log(Level.SEVERE, null, ex);
            return new LinkedList<Book>();
        }
    }
}
