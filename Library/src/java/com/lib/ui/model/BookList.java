/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lib.ui.model;

import com.opensymphony.xwork2.ActionSupport;
import com.lib.da.BookManager;
import com.lib.enitity.Book;
import java.util.List;

/**
 *
 * @author Shuuyu
 */
public class BookList extends ActionSupport {
    private String keyword;
    private List<Book> books;
    
    @Override
    public String execute() throws Exception {
        books = new BookManager().getBooksByName(keyword);
        return SUCCESS;
    }
    public void setKeyword(String keyword){
        this.keyword = keyword;
    }
    public String getKeyword(){
        return keyword;
    }
    public List<Book> getBooks(){
        return books;
    }
    
}
